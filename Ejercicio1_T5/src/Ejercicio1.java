
public class Ejercicio1 {

	public static void main(String[] args) {
		String  fragmentoNombreAlumnos = "Victor";
		String [] nombreAlumnos =  {"Antonio", "Marta", "Victor Hugo", "David"};
	 int resultado = contarUsuarios (fragmentoNombreAlumnos, nombreAlumnos); //cuenta el n�mero de alumnos que hay.
		
		System.out.println("Total resultados: " + resultado);
				
	}

	static int contarUsuarios (String fragmentoNombreAlumno, String[] nombreAlumnos) { //crea una funci�n est�tica con un valor de retorno totalEncontrados
		boolean encontrado= false;//hace una asignaci�n y el valor de retorno es el nuevo valor
		int totalEncontrados = 0;//en caso de ser falso el 0 va a ser el valor que retorne la funci�n
		
		for (String nombreAlumnoActual : nombreAlumnos) {//la cadena  nombreAlumno = nombreAlumnos
			if (nombreAlumnoActual.contains(fragmentoNombreAlumno)) {//si el nombreAlumnoActual contiene uno de los nombres fragmentoNombreAlumno, devuelve un 1
				encontrado = true;
			}if(encontrado) { 
				totalEncontrados ++;
				encontrado = false;
			}
		}
	return totalEncontrados;
	}
}
